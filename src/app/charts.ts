import { makeChart } from '../components/chart';
import { ISrcData, SrcTypes } from '../data/srcData';
import { ChartType, IChartData } from '../components/types';

export function renderCharts(
  chartsData: any[],
  containerElement: HTMLElement,
): void {
  chartsData.forEach(rawChartData => {
    const formattedChartData = makeChartData(rawChartData);
    const chart = makeChart(formattedChartData);
    chart.render(containerElement);
  });
}

function makeChartData(rawCharData: ISrcData): IChartData {
  const chartData: IChartData = {
    xAxis: [],
    dataSets: [],
  };
  const { columns, types, names, colors } = rawCharData;

  columns.forEach((column, index) => {
    const name = <string>column[0];
    const values = <number[]>column.slice(1);

    if (types[name] === SrcTypes.x) {
      chartData.xAxis = values;
      return;
    }

    chartData.dataSets.push({
      values,
      id: index,
      name: names[name],
      type: ChartType.line,
      color: colors[name],
      isVisible: true,
    });
  });

  return chartData;
}
