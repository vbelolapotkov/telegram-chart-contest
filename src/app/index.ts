import { ISrcData } from '../data/srcData';
import { makeHtmlElement } from '../utils/ui';
import { renderThemeSwitcher } from './theme-switcher';
import { renderCharts } from './charts';

export function renderApp(chartsData: ISrcData[]): void {
  const rootElement = renderRootElement();
  renderThemeSwitcher(rootElement);
  renderCharts(chartsData, rootElement);
}

function renderRootElement(): HTMLElement {
  const rootElement = createRootElement();
  document.body.appendChild(rootElement);
  return rootElement;
}

function createRootElement(): HTMLElement {
  const rootElement = makeHtmlElement('div', {
    attributes: { id: 'root' },
  });
  rootElement.innerHTML = '<h1>Followers</h1>';
  return rootElement;
}
