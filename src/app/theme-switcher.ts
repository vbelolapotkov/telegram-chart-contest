import { makeHtmlElement } from '../utils/ui';

interface IThemeSwitcher {
  readonly render: (container: HTMLElement) => void;
}

export function renderThemeSwitcher(container: HTMLElement): void {
  const switcher = makeThemeSwitcher();
  switcher.render(container);
}

function makeThemeSwitcher(): IThemeSwitcher {
  let currentTheme = 'light';
  let control: HTMLElement;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement) {
    const switcher = makeHtmlElement('div', {
      classList: ['theme-switcher'],
    });

    control = makeHtmlElement('a', { attributes: { href: '#' } });
    updateControl();
    control.addEventListener('click', handleClick);

    switcher.appendChild(control);
    container.appendChild(switcher);
  }

  function handleClick(evt: MouseEvent) {
    evt.preventDefault();
    toggleTheme();
    updateControl();
  }

  function toggleTheme() {
    if (currentTheme === 'dark') {
      document.body.classList.remove('dark');
      currentTheme = 'light';
    } else {
      document.body.classList.add('dark');
      currentTheme = 'dark';
    }
  }

  function updateControl() {
    control.innerText =
      currentTheme === 'dark' ? 'Switch to day mode' : 'Switch to night mode';
  }
}
