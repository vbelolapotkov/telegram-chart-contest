import { IAttributesMap, makeSvgRootElement } from '../utils/ui';
import { ISize, ChartComponentFactory } from './types';
import { makeYAxis } from './y-axis';
import { makeXAxis } from './x-axis';
import { makePlotsInspector } from './plots-inspector';
import { IPlots, makePlots } from './plots';

export const makeChartPlots: ChartComponentFactory<ISize> = function(
  chartStateController,
) {
  const stateController = chartStateController;
  const xAxisHeight = 30;

  let width = 0;
  let height = 0;
  let svg: SVGElement;
  let htmlContainer: HTMLElement;
  let plots: IPlots;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement, size: ISize) {
    initProps(container, size);
    renderPlotContainer();
    renderAxes();
    renderPlots();
    renderInspector();
    attachEventHandlers();
  }

  function initProps(container: HTMLElement, size: ISize) {
    htmlContainer = container;
    width = size.width;
    height = size.height;
  }

  function renderPlotContainer() {
    svg = makeSvgRootElement({
      classList: ['plots-container'],
      attributes: getAttributes(),
    });

    htmlContainer.appendChild(svg);
  }

  function renderAxes() {
    const plotSize = getPlotsSize();
    const yAxis = makeYAxis(stateController);
    yAxis.render(svg, { plotSize });
    const xAxis = makeXAxis(stateController);
    const axisSize = { width, height: xAxisHeight };
    xAxis.render(svg, { plotSize, axisSize });
  }

  function renderPlots() {
    const chartState = stateController.getState();
    const plotSize = getPlotsSize();
    plots = makePlots();
    plots.render(svg, {
      size: plotSize,
      xTotalRange: chartState.xTotalRange,
      yTotalRange: chartState.yTotalRange,
      xScale: chartState.xScale,
      xOffset: chartState.xOffset,
      yScale: chartState.yScale,
      yOffset: chartState.yOffset,
    });
    plots.renderDataSets(chartState.xAxis, chartState.dataSets);
  }

  function renderInspector() {
    const plotsInspector = makePlotsInspector(stateController);
    const plotSize = getPlotsSize();
    plotsInspector.render(svg, { plotSize, tooltipContainer: htmlContainer });
  }

  function getAttributes(): IAttributesMap {
    const plotSize = getPlotsSize();

    return {
      width: `${width}`,
      height: `${height}`,
      preserveAspectRatio: 'none',
      viewBox: `0 ${-plotSize.height} ${width} ${height}`,
    };
  }

  function getPlotsSize(): ISize {
    return {
      width,
      height: height - xAxisHeight,
    };
  }

  function attachEventHandlers() {
    stateController.addListener(
      'dataSetVisibilityChanged',
      handleDataSetVisibilityChanged,
    );
    stateController.addListener('xViewRangeChanged', handleXViewChanged);
    stateController.addListener('yViewRangeChanged', handleYViewRangeChanged);
  }

  function handleDataSetVisibilityChanged() {
    const { dataSets, yScale, yOffset } = stateController.getState();
    plots.updateDataSetsVisibility(dataSets);
    plots.setYViewRange(yScale, yOffset);
  }

  function handleXViewChanged(): void {
    const { xScale, xOffset } = stateController.getState();
    plots.setXViewRange(xScale, xOffset);
  }

  function handleYViewRangeChanged() {
    const { yScale, yOffset } = stateController.getState();
    plots.setYViewRange(yScale, yOffset);
  }
};
