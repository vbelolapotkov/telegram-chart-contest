import { makeHtmlElement, toPx } from '../utils/ui';
import { ChartComponentFactory, ISize } from './types';
import { makePreviewPlots } from './preview-plots';
import { makePreviewWindow } from './preview-window';

export const makeChartPreview: ChartComponentFactory<ISize> = function(
  chartStateController,
) {
  const stateController = chartStateController;

  let previewContainer: HTMLElement;
  let previewWidth = 0;
  let previewHeight = 0;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement, props: ISize): void {
    previewWidth = props.width;
    previewHeight = props.height;

    previewContainer = makePreviewAreaContainer();
    container.appendChild(previewContainer);

    renderPlots();
    renderPreviewWindow();
  }

  function makePreviewAreaContainer(): HTMLElement {
    return makeHtmlElement('div', {
      classList: ['preview-container'],
      style: { height: toPx(previewHeight) },
    });
  }

  function renderPlots() {
    const previewPlots = makePreviewPlots(stateController);
    previewPlots.render(previewContainer, {
      width: previewWidth,
      height: previewHeight,
    });
  }

  function renderPreviewWindow(): void {
    const previewWindow = makePreviewWindow(stateController);
    previewWindow.render(previewContainer, {
      width: previewWidth,
      height: previewHeight,
    });
  }
};
