import {
  ChartEventListener,
  IChartState,
  IChartStateController,
  IDataSet,
} from './types';

export function makeChartStateController(
  chartState: IChartState,
): IChartStateController {
  const state = chartState;
  const listeners: Map<string, ChartEventListener[]> = new Map();

  return Object.freeze({
    getState,
    changeXViewRange,
    toggleDataSetVisibility,
    addListener,
    showInspector,
    hideInspector,
    toggleInspector,
    moveInspector,
  });

  function getState() {
    return state;
  }

  function addListener(eventName: string, listener: ChartEventListener) {
    const eventListeners = listeners.get(eventName) || [];
    eventListeners.push(listener);
    listeners.set(eventName, eventListeners);
  }

  function fireEvent(eventName: string) {
    const eventListeners = listeners.get(eventName) || [];
    eventListeners.forEach(listener => listener(state));
  }

  function changeXViewRange(xScale?: number, xOffset?: number) {
    const xScaleChanged = typeof xScale === 'number' && state.xScale !== xScale;
    const xOffsetChanged =
      typeof xOffset === 'number' && state.xOffset !== xOffset;
    if (!xScaleChanged && !xOffsetChanged) {
      return;
    }

    const nextXScale = xScaleChanged ? xScale : state.xScale;
    const nextXOffset = xOffsetChanged ? xOffset : state.xOffset;
    state.setXViewRange(nextXScale, nextXOffset);
    hideInspector();
    fireEvent('xViewRangeChanged');
    fireEvent('yViewRangeChanged');
  }

  function toggleDataSetVisibility(dataSet: IDataSet) {
    state.toggleDataSetVisibility(dataSet);
    state.setInspectorVisibility(false);
    hideInspector();
    fireEvent('yViewRangeChanged');
    fireEvent('dataSetVisibilityChanged');
  }

  function showInspector() {
    state.setInspectorVisibility(true);
    fireEvent('inspectorVisibilityChanged');
  }

  function hideInspector() {
    state.setInspectorVisibility(false);
    fireEvent('inspectorVisibilityChanged');
  }

  function toggleInspector() {
    state.setInspectorVisibility(!state.inspectorIsActive);
    fireEvent('inspectorVisibilityChanged');
  }

  function moveInspector(xOffset: number) {
    state.moveInspector(xOffset);
    state.setInspectorVisibility(true);
    fireEvent('moveInspector');
  }
}
