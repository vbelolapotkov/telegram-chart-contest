import {
  IChartData,
  IChartState,
  IDataSet,
  IInspectorValue,
  IInspectorValues,
  IRange,
} from './types';
import { adjustRange, getAdjustedYRange } from '../utils/state';

export function makeChartState(chartData: IChartData): IChartState {
  const yLabelsCount = 6;
  const { xAxis, dataSets } = chartData;
  const xTotalRange = getXTotalRange();
  const dataSetsYRange: IRange[] = getDataSetsYRange();
  const yTotalRange = getYTotalRange();
  const yLabels = getYLabels(yTotalRange);

  let inspectorIndex = 0;

  const state = {
    xAxis,
    dataSets,
    xTotalRange,
    yTotalRange,
    yLabels,
    setXViewRange,
    toggleDataSetVisibility,
    setInspectorVisibility,
    moveInspector,
    inspectorIsActive: false,
    inspectorValues: getInspectorValues(),
    xScale: 0.3,
    xOffset: 0.7,
    yScale: 1,
    yOffset: 0,
    previewYScale: 1,
  };

  updateYViewRange();

  return state;

  function getXTotalRange(): IRange {
    const start = xAxis[0];
    const end = xAxis[xAxis.length - 1];
    const distance = end - start;
    return { start, end, distance };
  }

  function getDataSetsYRange() {
    return dataSets.map(dataSet => getAdjustedYRange(dataSet, yLabelsCount));
  }

  function getYTotalRange(startIndex?: number, endIndex?: number): IRange {
    let start = 0;
    let end = 1;
    dataSets.forEach(dataSet => {
      if (!dataSet.isVisible) {
        return;
      }

      const startAt =
        typeof startIndex === 'number' ? Math.max(startIndex, 0) : 0;
      const maxIndex = dataSet.values.length - 1;
      const endAt =
        typeof endIndex === 'number' ? Math.min(endIndex, maxIndex) : maxIndex;

      for (let i = startAt; i < endAt + 1; i = i + 1) {
        const value = dataSet.values[i];
        if (value < start) {
          start = value;
        }

        if (value > end) {
          end = value;
        }
      }
    });

    const distance = end - start;
    const range = { start, end, distance };
    return adjustRange(range, yLabelsCount);
  }

  function getYLabels(range: IRange): number[] {
    const labelStep = range.distance / yLabelsCount;
    let label = range.start;
    const labels = [label];
    for (let i = 1; i < yLabelsCount; i = i + 1) {
      label = label + labelStep;
      labels.push(label);
    }

    return labels;
  }

  function getYViewRange(): IRange {
    const xIndexStart = findXViewStartIndex();
    const xIndexEnd = findXViewEndIndex(xIndexStart);
    return getYTotalRange(xIndexStart, xIndexEnd);
  }

  function setXViewRange(xScale: number, xOffset: number) {
    state.xScale = xScale;
    state.xOffset = xOffset;
    updateYViewRange();
  }

  function findXViewStartIndex(): number {
    const { xOffset, xTotalRange, xAxis } = state;
    const preciseStart = xTotalRange.start + xOffset * xTotalRange.distance;
    const maxIndex = xAxis.length - 1;
    let index = Math.floor(maxIndex * xOffset);
    let value = xAxis[index];

    if (value < preciseStart) {
      while (index < maxIndex && value < preciseStart) {
        index = index + 1;
        value = xAxis[index];
      }
    } else {
      while (index > 0 && value >= preciseStart) {
        index = index - 1;
        value = xAxis[index];
      }
    }

    return index;
  }

  function findXViewEndIndex(startIndex: number): number {
    const { xScale, xOffset, xTotalRange, xAxis } = state;
    const preciseEnd =
      xTotalRange.start + (xOffset + xScale) * xTotalRange.distance;
    const maxIndex = xAxis.length - 1;
    let index = Math.ceil(maxIndex * (xOffset + xScale));
    let value = xAxis[index];

    if (value < preciseEnd) {
      while (index < maxIndex && value < preciseEnd) {
        index = index + 1;
        value = xAxis[index];
      }
    } else {
      while (index > startIndex && value >= preciseEnd) {
        index = index - 1;
        value = xAxis[index];
      }
    }

    return index;
  }

  function toggleDataSetVisibility(dataSet: IDataSet): void {
    const currentDataSet = dataSets.find(dS => dS.id === dataSet.id);
    currentDataSet.isVisible = !currentDataSet.isVisible;
    updateYViewRange();
    updatePreviewYScale();
  }

  function updatePreviewYScale() {
    const range = getYPreviewRange();
    state.previewYScale = range.distance / yTotalRange.distance;
  }

  function getYPreviewRange(): IRange {
    let range: IRange;

    state.dataSets.forEach((dataSet, index) => {
      if (!dataSet.isVisible) {
        return;
      }

      const dataSetRange = dataSetsYRange[index];

      if (!range) {
        range = dataSetRange;
        return;
      }

      range.start = Math.min(range.start, dataSetRange.start);
      range.end = Math.max(range.end, dataSetRange.end);
    });

    if (!range) {
      range = {
        start: 0,
        end: 1,
        distance: 1,
      };
    } else {
      range.distance = range.end - range.start;
    }

    return range;
  }

  function updateYViewRange(): void {
    const yViewRange = getYViewRange();
    state.yScale = yViewRange.distance / yTotalRange.distance;
    state.yOffset = yViewRange.start;
    state.yLabels = getYLabels(yViewRange);
  }

  function setInspectorVisibility(isActive: boolean) {
    state.inspectorIsActive = isActive;
  }

  function moveInspector(relativeXOffset: number) {
    const maxIndex = xAxis.length - 1;
    inspectorIndex = Math.round(
      state.xOffset * maxIndex + relativeXOffset * maxIndex * state.xScale,
    );
    inspectorIndex = Math.max(0, inspectorIndex);
    inspectorIndex = Math.min(inspectorIndex, xAxis.length - 1);
    state.inspectorValues = getInspectorValues();
  }

  function getInspectorValues(): IInspectorValues {
    const xValue = xAxis[inspectorIndex];
    const yValues: IInspectorValue[] = dataSets.map(dataSet => ({
      name: dataSet.name,
      color: dataSet.color,
      value: dataSet.values[inspectorIndex],
      isVisible: dataSet.isVisible,
    }));

    return { xValue, yValues };
  }
}
