import { IChartData, IChartComponent } from './types';
import { makeChartState } from './chart-state';
import { makePlotArea } from './plot-area';
import { makeChartPreview } from './chart-preview';
import { makeLegend } from './legend';

import { makeHtmlElement } from '../utils/ui';
import { makeChartStateController } from './chart-state-controller';

export function makeChart(chartData: IChartData): IChartComponent<null> {
  const chartState = makeChartState(chartData);
  const chartStateController = makeChartStateController(chartState);
  const chartAspectRatio = 4 / 3;
  const previewAspectRatio = 5;

  let chartContainer: HTMLElement;
  let plotHeight: number;
  let plotWidth: number;
  let previewHeight: number;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement): void {
    renderChartContainer(container);
    renderPlotArea();
    renderPreview();
    renderLegend();
  }

  function renderChartContainer(container: HTMLElement) {
    chartContainer = makeChartContainer();
    container.appendChild(chartContainer);
    initSizeProps(chartContainer);
  }

  function makeChartContainer(): HTMLElement {
    return makeHtmlElement('div', {
      classList: ['chart-container'],
    });
  }

  function initSizeProps(chartContainer: HTMLElement): void {
    plotWidth = chartContainer.offsetWidth;
    plotHeight = Math.round(plotWidth / chartAspectRatio);
    previewHeight = Math.round(plotHeight / previewAspectRatio);
  }

  function renderPlotArea() {
    const plotArea = makePlotArea(chartStateController);
    plotArea.render(chartContainer, { plotWidth, plotHeight });
  }

  function renderPreview() {
    const preview = makeChartPreview(chartStateController);
    preview.render(chartContainer, {
      width: plotWidth,
      height: previewHeight,
    });
  }

  function renderLegend() {
    const legend = makeLegend(chartStateController);
    legend.render(chartContainer);
  }
}
