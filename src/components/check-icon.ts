import checkSolidSvg from '../assets/check-solid.svg';
import { makeHtmlElement } from '../utils/ui';

export function makeCheckIcon(): HTMLElement {
  const el = makeHtmlElement('i', {
    classList: ['icon'],
  });

  el.innerHTML = checkSolidSvg;
  return el;
}
