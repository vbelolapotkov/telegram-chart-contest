import { DOMContainerElement, ChartComponentFactory, IDataSet } from './types';
import { applyElementConfig, makeHtmlElement } from '../utils/ui';
import { makeCheckIcon } from './check-icon';

export const makeLegend: ChartComponentFactory<null> = function(
  chartStateController,
) {
  const stateController = chartStateController;
  let controlsContainer: HTMLElement;

  return Object.freeze({
    render,
  });

  function render(container: DOMContainerElement) {
    const legendContainer = makeLegendContainer();
    const { dataSets } = stateController.getState();
    controlsContainer = makeControlsContainer();
    dataSets.forEach(dataSet => {
      const dataSetVisibilityControl = makeVisibilityControl(dataSet);
      controlsContainer.appendChild(dataSetVisibilityControl);
    });
    legendContainer.appendChild(controlsContainer);
    container.appendChild(legendContainer);
    stateController.addListener(
      'dataSetVisibilityChanged',
      handleVisibilityChanged,
    );
  }

  function makeLegendContainer(): HTMLElement {
    return makeHtmlElement('div', {
      classList: ['legend-container'],
    });
  }

  function makeControlsContainer(): HTMLElement {
    return makeHtmlElement('div', {
      classList: ['controls-list'],
    });
  }

  function makeVisibilityControl(dataSet: IDataSet): HTMLElement {
    const { id, name, color } = dataSet;
    const control = makeHtmlElement('div', {
      classList: ['control'],
      attributes: {
        'data-setId': `${id}`,
      },
    });

    const checkbox = makeHtmlElement('div', {
      classList: ['checkbox', 'checked'],
      style: {
        backgroundColor: color,
        'border-color': color,
      },
    });

    checkbox.appendChild(makeCheckIcon());

    const label = makeHtmlElement('div', {
      classList: ['label'],
    });
    label.innerText = name;

    control.appendChild(checkbox);
    control.appendChild(label);

    control.addEventListener('click', () =>
      stateController.toggleDataSetVisibility(dataSet),
    );

    return control;
  }

  function handleVisibilityChanged() {
    const { dataSets } = stateController.getState();
    dataSets.forEach(updateVisibilityControlState);
  }

  function updateVisibilityControlState(dataSet: IDataSet) {
    const { id, isVisible, color } = dataSet;
    const checkbox = <HTMLElement>(
      controlsContainer.querySelector(`[data-setId="${id}"] .checkbox`)
    );
    if (!checkbox) {
      return;
    }

    if (isVisible) {
      applyElementConfig(checkbox, {
        classList: ['checked'],
        style: {
          backgroundColor: color,
        },
      });
    } else {
      checkbox.classList.remove('checked');
      checkbox.style.backgroundColor = '';
    }
  }
};
