import { DOMContainerElement, ChartComponentFactory, ISize } from './types';
import { makeChartPlots } from './chart-plots';
import { makeHtmlElement } from '../utils/ui';

interface IPlotAreaProps {
  plotWidth: number;
  plotHeight: number;
}

export const makePlotArea: ChartComponentFactory<IPlotAreaProps> = function(
  chartStateController,
) {
  const stateController = chartStateController;
  let plotSize: ISize;
  let touchStartTime: number;
  const tapThresh = 100;

  return {
    render,
  };

  function render(container: DOMContainerElement, props: IPlotAreaProps): void {
    const { plotWidth: width, plotHeight: height } = props;
    const plotAreaContainer = makePlotAreaContainer();
    plotSize = { width, height };
    const chartPlots = makeChartPlots(stateController);
    chartPlots.render(plotAreaContainer, plotSize);
    container.appendChild(plotAreaContainer);

    attachInspectorHandlers(plotAreaContainer);
  }

  function makePlotAreaContainer(): HTMLElement {
    return makeHtmlElement('div', {
      classList: ['plot-area-container'],
    });
  }

  function attachInspectorHandlers(container: HTMLElement) {
    container.addEventListener('mouseenter', handleMouseEnter);
    container.addEventListener('mouseleave', handleMouseLeave);
    container.addEventListener('mousemove', handleMouseMove);

    container.addEventListener('touchstart', handleTouchStart);
    container.addEventListener('touchend', handleTouchEnd);
    container.addEventListener('touchcancel', handleTouchEnd);
    container.addEventListener('touchmove', handleTouchMove);
  }

  function handleMouseEnter() {
    stateController.showInspector();
  }

  function handleMouseLeave() {
    stateController.hideInspector();
  }

  function handleMouseMove(evt: MouseEvent) {
    moveInspector(evt.offsetX);
  }

  function handleTouchStart() {
    touchStartTime = Date.now();
  }

  function handleTouchEnd() {
    const now = Date.now();
    if (now - touchStartTime < tapThresh) {
      stateController.toggleInspector();
    }
  }

  function handleTouchMove(evt: TouchEvent) {
    if (evt.touches.length !== 1) {
      return;
    }

    const { clientX } = evt.touches[0];
    const { left, width } = (<HTMLElement>(
      evt.currentTarget
    )).getBoundingClientRect();

    const plotX = clientX - left;

    if (plotX < 0 || plotX > width) {
      return;
    }

    moveInspector(plotX);
  }

  function moveInspector(plotX: number) {
    const xOffset = plotX / plotSize.width;
    stateController.moveInspector(xOffset);
  }
};
