import {
  DOMContainerElement,
  ISize,
  IInspectorValue,
  ChartComponentFactory,
} from './types';
import {
  applyElementConfig,
  IAttributesMap,
  makeHtmlElement,
  makeSvgElement,
} from '../utils/ui';

interface IPlotsInspectorProps {
  plotSize: ISize;
  tooltipContainer: HTMLElement;
}

export const makePlotsInspector: ChartComponentFactory<
  IPlotsInspectorProps
> = function(chartStateController) {
  const stateController = chartStateController;
  const svgPointRadius = '5';

  let svgInspector: SVGElement;
  let svgInspectorLine: SVGElement;
  const svgPoints: SVGElement[] = [];
  let tooltipContainer: HTMLElement;
  let tooltipElement: HTMLElement;
  let plotSize: ISize;
  let xCoordsScale: number;
  let yCoordsScale: number;
  let inspectorXCoordinate: number;

  return Object.freeze({
    render,
  });

  function render(container: DOMContainerElement, props: IPlotsInspectorProps) {
    initInspectorProps(props);
    renderInspector(container);
    attachEventHandlers();
  }

  function initInspectorProps(props: IPlotsInspectorProps) {
    plotSize = props.plotSize;
    tooltipContainer = props.tooltipContainer;
    const { xTotalRange, yTotalRange } = stateController.getState();
    xCoordsScale = plotSize.width / xTotalRange.distance;
    yCoordsScale = plotSize.height / yTotalRange.distance;
    updateInspectorXCoordinate();
  }

  function renderInspector(container: DOMContainerElement) {
    renderSvgInspector(container);
    renderSvgInspectorLine();
    renderSvgInspectorPoints();
    renderSvgInspectorTooltip();
  }

  function renderSvgInspector(container: DOMContainerElement) {
    svgInspector = makeSvgElement('g', {
      classList: ['plots-inspector'],
      style: { opacity: getInspectorOpacity() },
    });
    container.appendChild(svgInspector);
  }

  function renderSvgInspectorLine() {
    svgInspectorLine = makeSvgElement('line', {
      classList: ['inspector-line'],
      attributes: getInspectorLineAttributes(),
    });

    svgInspector.appendChild(svgInspectorLine);
  }

  function renderSvgInspectorPoints() {
    const {
      inspectorValues: { yValues },
    } = stateController.getState();
    yValues.forEach(yValue => {
      const svgPoint = makeSvgElement('circle', {
        classList: ['inspector-point'],
        attributes: getSvgPointAttributes(yValue),
        style: getSvgPointStyle(yValue),
      });

      svgPoints.push(svgPoint);
      svgInspector.appendChild(svgPoint);
    });
  }

  function renderSvgInspectorTooltip() {
    tooltipElement = makeHtmlElement('div', {
      classList: ['inspector-tooltip'],
    });

    tooltipElement.addEventListener('mouseenter', evt => evt.stopPropagation());
    tooltipElement.addEventListener('mouseleave', evt => evt.stopPropagation());
    tooltipElement.addEventListener('mousemove', evt => evt.stopPropagation());

    updateTooltip();
    tooltipContainer.appendChild(tooltipElement);
  }

  function getInspectorOpacity() {
    const { inspectorIsActive } = stateController.getState();
    return inspectorIsActive ? '1' : '0';
  }

  function updateInspectorXCoordinate() {
    const {
      inspectorValues,
      xScale,
      xOffset,
      xTotalRange,
    } = stateController.getState();

    const xOffsetValue = xTotalRange.start + xOffset * xTotalRange.distance;
    inspectorXCoordinate =
      ((inspectorValues.xValue - xOffsetValue) * xCoordsScale) / xScale;
  }

  function getInspectorLineAttributes(): IAttributesMap {
    return {
      x1: `${inspectorXCoordinate}`,
      x2: `${inspectorXCoordinate}`,
      y1: '0',
      y2: `${-plotSize.height}`,
    };
  }

  function getSvgPointAttributes(yValue: IInspectorValue): IAttributesMap {
    const { yOffset, yScale } = stateController.getState();
    const yCoordinate = ((yValue.value - yOffset) * yCoordsScale) / yScale;
    return {
      cx: `${inspectorXCoordinate}`,
      cy: `${-yCoordinate}`,
      r: svgPointRadius,
      stroke: yValue.color,
      'data-name': yValue.name,
    };
  }

  function getSvgPointStyle(yValue: IInspectorValue): IAttributesMap {
    return { opacity: yValue.isVisible ? '1' : '0' };
  }

  function attachEventHandlers() {
    stateController.addListener(
      'inspectorVisibilityChanged',
      updateInspectorVisibility,
    );
    stateController.addListener('moveInspector', updateInspector);
  }

  function updateInspectorVisibility() {
    updateInspectorLineVisibility();
    updateTooltip();
  }

  function updateInspectorLineVisibility() {
    applyElementConfig(svgInspector, {
      style: { opacity: getInspectorOpacity() },
    });
  }

  function updateInspector() {
    updateInspectorLineVisibility();
    updateInspectorXCoordinate();
    updateInspectorLine();
    updateInspectorPoints();
    updateTooltip();
  }

  function updateInspectorLine() {
    applyElementConfig(svgInspectorLine, {
      attributes: getInspectorLineAttributes(),
    });
  }

  function updateInspectorPoints() {
    const { inspectorValues } = stateController.getState();
    inspectorValues.yValues.forEach((yValue, index) =>
      applyElementConfig(svgPoints[index], {
        attributes: getSvgPointAttributes(yValue),
        style: getSvgPointStyle(yValue),
      }),
    );
  }

  function updateTooltip() {
    updateTooltipStyle();
    updateTooltipValues();
  }

  function updateTooltipStyle() {
    const style = getTooltipStyle();
    applyElementConfig(tooltipElement, { style });
  }

  function getTooltipStyle() {
    const { inspectorIsActive } = stateController.getState();

    if (!inspectorIsActive) {
      return { opacity: '0', left: '0' };
    }

    const { offsetWidth: tooltipWidth } = tooltipElement;
    const tooltipShift = 20;
    const minLeft = 2;

    let tooltipLeft = Math.max(
      Math.floor(inspectorXCoordinate - tooltipShift),
      minLeft,
    );

    if (tooltipLeft + tooltipWidth >= plotSize.width) {
      tooltipLeft = Math.floor(plotSize.width - minLeft - tooltipWidth);
    }

    return {
      left: `${tooltipLeft}px`,
      opacity: '1',
    };
  }

  function updateTooltipValues() {
    const {
      inspectorValues: { xValue, yValues },
    } = stateController.getState();
    const formattedDate = new Date(xValue).toLocaleString('en-US', {
      month: 'short',
      day: '2-digit',
      weekday: 'short',
    });

    tooltipElement.innerHTML = `
      <div class="date">${formattedDate}</div>
      <div class="values">${yValues
        .map(
          ({ value, color, name, isVisible }) =>
            `<div class="set ${
              isVisible ? '' : 'hidden'
            }" style="color: ${color};" >
              <div class="value">${value}</div>
              <div class="name">${name}</div>
            </div>`,
        )
        .join('')}</div> `;
  }
};
