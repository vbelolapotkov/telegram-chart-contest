import {
  DOMContainerElement,
  IChartComponent,
  ICoordinates,
  IDataSet,
  IRange,
  ISize,
  Timestamp,
} from './types';
import {
  applyElementConfig,
  IAttributesMap,
  makeLinearAnimation,
  makeSvgElement,
} from '../utils/ui';

interface IPlotsProps {
  size: ISize;
  xScale: number;
  yScale: number;
  xOffset: number;
  yOffset: number;
  xTotalRange: IRange;
  yTotalRange: IRange;
}

export interface IPlots extends IChartComponent<IPlotsProps> {
  renderDataSets: (xAxis: Timestamp[], dataSets: IDataSet[]) => void;
  setXViewRange: (xScale: number, xOffset: number) => void;
  setYViewRange: (yScale: number, yOffset: number) => void;
  updateDataSetsVisibility: (dataSets: IDataSet[]) => void;
}

export function makePlots(): IPlots {
  let svgPlots: SVGElement;
  let width = 0;
  let height = 0;
  let xScale = 1;
  let xOffset = 0;
  let yScale = 1;
  let yOffset = 0;
  let xTotalRange: IRange;
  let yTotalRange: IRange;
  let yCoordsScale: number;
  let xCoordsScale: number;

  let yAnimationInProgress: boolean;
  let yAnimationTargetScale: number;
  let yAnimationTargetOffset: number;
  let yAnimationStop: () => void;

  return Object.freeze({
    render,
    renderDataSets,
    setXViewRange,
    setYViewRange,
    updateDataSetsVisibility,
  });

  function render(container: DOMContainerElement, props: IPlotsProps) {
    const { size } = props;
    width = size.width;
    height = size.height;
    xScale = props.xScale;
    yScale = props.yScale;
    xOffset = props.xOffset;
    yOffset = props.yOffset;
    xTotalRange = props.xTotalRange;
    yTotalRange = props.yTotalRange;
    xCoordsScale = width / xTotalRange.distance;
    yCoordsScale = height / yTotalRange.distance;

    svgPlots = makeSvgElement('g', {
      attributes: getPlotsAttributes(),
    });

    container.appendChild(svgPlots);
  }

  function renderDataSets(xAxis: Timestamp[], dataSets: IDataSet[]) {
    dataSets.forEach(dataSet => renderDataSet(xAxis, dataSet));
  }

  function renderDataSet(xAxis: Timestamp[], dataSet: IDataSet): void {
    const points = getPoints(xAxis, dataSet);
    renderPlot(points, dataSet);
  }

  function getPoints(xAxis: Timestamp[], dataSet: IDataSet): ICoordinates[] {
    return xAxis.map((xValue, index) => {
      const yValue = dataSet.values[index];
      const x = getXCoordinate(xValue, xTotalRange.start, xCoordsScale);
      const y = getYCoordinate(yValue, yTotalRange.start, yCoordsScale);
      return { x, y };
    });
  }

  function getXCoordinate(
    xValue: number,
    xOffset: number,
    xCoordsScale: number,
  ): number {
    return Math.round(xCoordsScale * (xValue - xOffset));
  }

  function getYCoordinate(
    yValue: number,
    yOffset: number,
    yCoordsScale: number,
  ): number {
    return -Math.round(yCoordsScale * (yValue - yOffset));
  }

  function renderPlot(points: ICoordinates[], dataSet: IDataSet) {
    const { id, color, isVisible } = dataSet;
    const classList = ['plot'];
    if (!isVisible) {
      classList.push('hidden');
    }

    const g = makeSvgElement('g', {
      classList,
      style: { stroke: color },
      attributes: {
        'data-setId': `${id}`,
      },
    });

    svgPlots.appendChild(g);

    const linePoints = points.map(({ x, y }) => `${x},${y}`);
    renderLine(g, linePoints, color);
  }

  function renderLine(
    container: SVGElement,
    points: string[],
    color: string,
  ): void {
    const polyline = makeSvgElement('polyline', {
      attributes: { points: points.join(' ') },
      classList: ['plot-line'],
      style: { stroke: color },
    });
    container.appendChild(polyline);
  }

  function setXViewRange(scale: number, offset: number): void {
    xScale = scale;
    xOffset = offset;
    const attributes = getPlotsAttributes();
    applyElementConfig(svgPlots, { attributes });
  }

  function setYViewRange(scale: number, offset: number): void {
    if (scale === yScale && offset === yOffset) {
      return;
    }

    if (yAnimationInProgress) {
      if (
        yAnimationTargetOffset === offset &&
        yAnimationTargetScale === scale
      ) {
        return;
      }

      yAnimationStop && yAnimationStop();
    }

    animateYViewRangeChange(scale, offset);
  }

  function animateYViewRangeChange(targetScale: number, targetOffset: number) {
    yAnimationInProgress = true;
    yAnimationTargetScale = targetScale;
    yAnimationTargetOffset = targetOffset;
    yAnimationStop = makeLinearAnimation(
      [
        { start: yScale, end: targetScale },
        { start: yOffset, end: targetOffset },
      ],
      200,
      ([nextScale, nextOffset]) => updateYViewRange(nextScale, nextOffset),
      () => {
        yAnimationInProgress = false;
        yAnimationTargetOffset = null;
        yAnimationTargetScale = null;
        yAnimationStop = null;
      },
    );
  }

  function updateYViewRange(scale: number, offset: number) {
    yScale = scale;
    yOffset = offset;
    const attributes = getPlotsAttributes();
    applyElementConfig(svgPlots, { attributes });
  }

  function updateDataSetsVisibility(dataSets: IDataSet[]) {
    dataSets.forEach(dataSet => {
      const { id, isVisible } = dataSet;
      const plot = svgPlots.querySelector(`.plot[data-setId="${id}"]`);
      if (!plot) {
        return;
      }

      if (isVisible) {
        plot.classList.remove('hidden');
      } else {
        plot.classList.add('hidden');
      }
    });
  }

  function getPlotsAttributes(): IAttributesMap {
    const kX = 1 / xScale;
    const kY = 1 / yScale;
    const dY = -yOffset;
    const dX = -xOffset * width;

    return {
      preserveAspectRatio: 'none',
      transform: `scale(${kX}, ${kY}) translate(${dX} ${dY})`,
    };
  }
}
