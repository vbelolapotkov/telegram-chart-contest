import { ChartComponentFactory, ISize } from './types';
import { IAttributesMap, makeSvgRootElement } from '../utils/ui';
import { IPlots, makePlots } from './plots';

export const makePreviewPlots: ChartComponentFactory<ISize> = function(
  chartStateController,
) {
  const stateController = chartStateController;

  let svg: SVGElement;
  let plotSize: ISize;
  let plots: IPlots;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement, size: ISize) {
    plotSize = size;
    renderPlotContainer(container);
    renderPlots();
    attachEventHandlers();
  }

  function renderPlotContainer(container: HTMLElement) {
    svg = makeSvgRootElement({
      classList: ['plot-container'],
      attributes: getAttributes(),
    });

    container.appendChild(svg);
  }

  function renderPlots() {
    const chartState = stateController.getState();
    plots = makePlots();
    plots.render(svg, {
      xTotalRange: chartState.xTotalRange,
      yTotalRange: chartState.yTotalRange,
      size: plotSize,
      xScale: 1,
      xOffset: 0,
      yScale: chartState.previewYScale,
      yOffset: 0,
    });
    plots.renderDataSets(chartState.xAxis, chartState.dataSets);
  }

  function attachEventHandlers() {
    stateController.addListener(
      'dataSetVisibilityChanged',
      handleDataSetVisibilityChanged,
    );
  }

  function getAttributes(): IAttributesMap {
    const { width, height } = plotSize;
    return {
      width: `${width}`,
      height: `${height}`,
      preserveAspectRatio: 'none',
      viewBox: `0 ${-height} ${width} ${height}`,
    };
  }

  function handleDataSetVisibilityChanged() {
    const { dataSets, previewYScale } = stateController.getState();
    plots.updateDataSetsVisibility(dataSets);
    plots.setYViewRange(previewYScale, 0);
  }
};
