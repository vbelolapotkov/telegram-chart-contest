import { ChartComponentFactory, ISize } from './types';
import { applyElementConfig, makeHtmlElement, toPx } from '../utils/ui';
import { makeTransparentImage } from './transparent-image';

type EdgeTouchEventHandler = (args: {
  clientX: number;
  left: number;
  width: number;
}) => void;

type EdgeDragEventHandler = (offsetX: number) => void;

export const makePreviewWindow: ChartComponentFactory<ISize> = function(
  chartStateController,
) {
  const stateController = chartStateController;
  const minWidth = 60;

  let width: number;
  let height: number;

  let previewWindow: HTMLElement;
  let leftShadow: HTMLElement;
  let rightShadow: HTMLElement;
  let resizeWindow: HTMLElement;
  let edgesContainer: HTMLElement;
  let leftEdge: HTMLElement;
  let rightEdge: HTMLElement;

  return Object.freeze({
    render,
  });

  function render(container: HTMLElement, size: ISize) {
    initProps(size);
    renderContainer(container);
    renderResizeWindow();
    attachEventHandlers();
  }

  function initProps(size: ISize) {
    width = size.width;
    height = size.height;
  }

  function renderContainer(container: HTMLElement) {
    previewWindow = makeHtmlElement('div', {
      classList: ['preview-window'],
    });
    container.appendChild(previewWindow);
  }

  function renderResizeWindow() {
    const { left, right } = getResizeWindowEdgesCoords();

    resizeWindow = makeHtmlElement('div', {
      classList: ['window'],
      style: {
        left: toPx(left),
        right: toPx(right),
      },
    });
    previewWindow.appendChild(resizeWindow);

    renderShadows(left, right);
    renderResizeEdges();
  }

  function getResizeWindowEdgesCoords() {
    const { xScale, xOffset } = stateController.getState();
    const left = Math.round(width * xOffset);
    const previewWindowWidth = Math.round(width * xScale);
    const right = Math.max(width - left - previewWindowWidth, 0);
    return { left, right };
  }

  function renderShadows(left: number, right: number) {
    leftShadow = makeHtmlElement('div', {
      classList: ['shadow', 'left'],
      style: { width: toPx(left) },
    });

    rightShadow = makeHtmlElement('div', {
      classList: ['shadow', 'right'],
      style: { width: toPx(right) },
    });

    previewWindow.appendChild(leftShadow);
    previewWindow.appendChild(rightShadow);
  }

  function attachEventHandlers() {
    attachEdgesContainerHandlers();
    attachLeftEdgeHandlers();
    attachRightEdgeHandlers();
  }

  function attachEdgesContainerHandlers() {
    edgesContainer.addEventListener(
      'touchmove',
      makeEdgeTouchEventHandler(({ clientX, left, width }) => {
        const windowOffsetX = clientX - left - width / 2;
        moveResizeWindow(windowOffsetX);
      }),
    );

    edgesContainer.addEventListener('dragstart', evt => {
      setGhostImage(evt);
      edgesContainer.classList.add('dragging');
    });
    edgesContainer.addEventListener('dragend', () => {
      edgesContainer.classList.remove('dragging');
    });
    edgesContainer.addEventListener('drag', evt => {
      if (evt.target !== edgesContainer) {
        return;
      }

      const { offsetX, pageX, clientX } = evt;
      if (pageX === 0 || clientX === 0) {
        return;
      }

      const { offsetWidth: resizeWindowWidth } = resizeWindow;
      const windowOffsetX = offsetX - resizeWindowWidth / 2;

      moveResizeWindow(windowOffsetX);
    });
  }

  function attachLeftEdgeHandlers() {
    leftEdge.addEventListener('dragstart', setGhostImage);
    leftEdge.addEventListener('drag', makeEdgeDragHandler(moveLeftEdge));
    leftEdge.addEventListener(
      'touchmove',
      makeEdgeTouchEventHandler(({ clientX, left }) =>
        moveLeftEdge(clientX - left),
      ),
    );
  }
  function attachRightEdgeHandlers() {
    rightEdge.addEventListener('dragstart', setGhostImage);
    rightEdge.addEventListener('drag', makeEdgeDragHandler(moveRightEdge));
    rightEdge.addEventListener(
      'touchmove',
      makeEdgeTouchEventHandler(({ clientX, left, width }) =>
        moveRightEdge(clientX - left - width),
      ),
    );
  }

  function makeEdgeDragHandler(
    handler: EdgeDragEventHandler,
  ): EventHandlerNonNull {
    return (evt: DragEvent) => {
      evt.preventDefault();
      const { offsetX, pageX, clientX } = evt;
      if (pageX === 0 || clientX === 0) {
        return;
      }
      handler(offsetX);
    };
  }

  function makeEdgeTouchEventHandler(
    handler: EdgeTouchEventHandler,
  ): EventHandlerNonNull {
    return (evt: TouchEvent) => {
      evt.preventDefault();
      evt.stopPropagation();
      if (evt.touches.length !== 1) {
        return;
      }

      const { clientX } = evt.touches[0];
      const { left, width } = edgesContainer.getBoundingClientRect();

      handler({ clientX, left, width });
    };
  }

  function moveLeftEdge(offsetX: number) {
    const {
      offsetLeft: resizeWindowLeft,
      offsetWidth: resizeWindowWidth,
    } = resizeWindow;

    if (offsetX + minWidth >= resizeWindowWidth) {
      return;
    }

    const leftOffset = Math.max(resizeWindowLeft + offsetX, 0);

    applyElementConfig(leftShadow, {
      style: { width: toPx(leftOffset) },
    });
    applyElementConfig(resizeWindow, {
      style: { left: toPx(leftOffset) },
    });

    const previewWindowWidth = resizeWindow.offsetWidth;
    const xScale = previewWindowWidth / width;
    stateController.changeXViewRange(xScale, getXOffset(leftOffset));
  }

  function moveRightEdge(offsetX: number) {
    const totalWidth = resizeWindow.parentElement.offsetWidth;
    const {
      offsetLeft: resizeWindowLeft,
      offsetWidth: resizeWindowWidth,
    } = resizeWindow;

    const offsetRight = totalWidth - (resizeWindowLeft + resizeWindowWidth);
    const newRight = offsetRight - offsetX;

    if (resizeWindowLeft + newRight + minWidth >= totalWidth) {
      return;
    }

    const rightOffset = Math.max(newRight, 0);

    applyElementConfig(rightShadow, {
      style: { width: toPx(rightOffset) },
    });
    applyElementConfig(resizeWindow, {
      style: { right: toPx(rightOffset) },
    });

    const xScale = resizeWindowWidth / width;
    stateController.changeXViewRange(xScale);
  }

  function moveResizeWindow(offsetX: number) {
    const {
      offsetLeft: resizeWindowLeft,
      offsetWidth: resizeWindowWidth,
    } = resizeWindow;

    const totalWidth = resizeWindow.parentElement.offsetWidth;

    let newLeft = Math.max(resizeWindowLeft + offsetX, 0);

    let newRight = totalWidth - newLeft - resizeWindowWidth;
    if (newRight < 0) {
      newRight = 0;
      newLeft = totalWidth - resizeWindowWidth;
    }

    applyElementConfig(leftShadow, {
      style: { width: toPx(newLeft) },
    });
    applyElementConfig(resizeWindow, {
      style: { left: toPx(newLeft) },
    });

    applyElementConfig(rightShadow, {
      style: { width: toPx(newRight) },
    });
    applyElementConfig(resizeWindow, {
      style: { right: toPx(newRight) },
    });

    stateController.changeXViewRange(undefined, getXOffset(newLeft));
  }

  function renderResizeEdges(): void {
    const dragAttributes = { draggable: 'true' };

    edgesContainer = makeHtmlElement('div', {
      classList: ['edges-container'],
      attributes: dragAttributes,
    });

    leftEdge = makeHtmlElement('div', {
      classList: ['edge', 'left'],
      attributes: dragAttributes,
    });

    rightEdge = makeHtmlElement('div', {
      classList: ['edge', 'right'],
      attributes: dragAttributes,
    });

    edgesContainer.appendChild(leftEdge);
    edgesContainer.appendChild(rightEdge);
    resizeWindow.appendChild(edgesContainer);
  }

  function getXOffset(leftOffset: number): number {
    return leftOffset / width;
  }

  function setGhostImage(evt: DragEvent) {
    evt.dataTransfer.setDragImage(makeTransparentImage(), 0, 0);
  }
};
