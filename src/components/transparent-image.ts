// tslint:disable-next-line:max-line-length
const transparentImgUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';

let img: HTMLImageElement;
export function makeTransparentImage(): HTMLElement {
  if (img) {
    return img;
  }

  img = new Image(1, 1);
  img.src = transparentImgUrl;
  document.body.appendChild(img);
  return img;
}
