export type Timestamp = number;

export enum ChartType {
  line = 'line',
}

export interface IChartData {
  xAxis: Timestamp[];
  dataSets?: IDataSet[];
}

export interface IDataSet {
  id: number;
  type: ChartType;
  name: string;
  color?: string;
  values: number[];
  isVisible: boolean;
}

export interface IBaseRange {
  start: number;
  end: number;
}

export interface IRange extends IBaseRange {
  distance: number;
}

export interface IInspectorValues {
  xValue: Timestamp;
  yValues: IInspectorValue[];
}

export interface IInspectorValue {
  name: string;
  color: string;
  value: number;
  isVisible: boolean;
}

export interface ISize {
  width: number;
  height: number;
}

export interface ICoordinates {
  x: number;
  y: number;
}

export interface IChartState {
  readonly xAxis: Timestamp[];
  readonly dataSets: IDataSet[];
  readonly xTotalRange: IRange;
  readonly yTotalRange: IRange;
  xScale: number;
  xOffset: number;
  yScale: number;
  yOffset: number;
  previewYScale: number;
  yLabels: number[];
  inspectorIsActive: boolean;
  inspectorValues: IInspectorValues;
  setXViewRange: (xScale: number, xOffset: number) => void;
  toggleDataSetVisibility: (dataSet: IDataSet) => void;
  setInspectorVisibility: (isActive: boolean) => void;
  moveInspector: (relativeXOffset: number) => void;
}

export interface IChartStateController {
  getState: () => IChartState;
  changeXViewRange: (xScale: number, xOffset?: number) => void;
  toggleDataSetVisibility: (dataSet: IDataSet) => void;
  addListener: (eventName: string, listener: ChartEventListener) => void;
  showInspector: () => void;
  hideInspector: () => void;
  toggleInspector: () => void;
  moveInspector: (xOffset: number) => void;
}

export type ChartEventListener = (chartState: IChartState) => void;

export type ChartComponentFactory<TProps> = (
  stateController: IChartStateController,
) => IChartComponent<TProps>;

export type DOMContainerElement = HTMLElement | SVGElement;

export interface IChartComponent<TProps> {
  render: (container: DOMContainerElement, props?: TProps) => void;
}
