import {
  DOMContainerElement,
  IChartStateController,
  ISize,
  IChartComponent,
  Timestamp,
} from './types';

import { applyElementConfig, makeSvgElement } from '../utils/ui';

interface IXAxisProps {
  plotSize: ISize;
  axisSize: ISize;
}

export function makeXAxis(
  chartStateController: IChartStateController,
): IChartComponent<IXAxisProps> {
  const stateController = chartStateController;
  const dayRange = 24 * 60 * 60 * 1000;
  const xTextLabelOffset = 25;
  const xLabelWidth = 3 * xTextLabelOffset;

  let xSvgLabels: SVGElement;

  let plotSize: ISize;
  let axisSize: ISize;
  let xCoordsScale: number;
  let xLabelsCount: number;

  return Object.freeze({ render });

  function render(container: DOMContainerElement, props: IXAxisProps): void {
    initAxisProps(props);
    renderAxis(container);
    attachHandlers();
  }

  function initAxisProps(props: IXAxisProps) {
    plotSize = props.plotSize;
    axisSize = props.axisSize;

    const { xTotalRange } = stateController.getState();
    xCoordsScale = plotSize.width / xTotalRange.distance;
    xLabelsCount = Math.floor(plotSize.width / xLabelWidth);
  }

  function renderAxis(container: DOMContainerElement): void {
    xSvgLabels = makeLabelsContainer();
    container.appendChild(xSvgLabels);
    createLabels();
    updateLabels();
  }

  function attachHandlers() {
    stateController.addListener('xViewRangeChanged', updateLabels);
  }

  function makeLabelsContainer(): SVGElement {
    return makeSvgElement('g', {
      classList: ['axis-labels', 'x'],
      attributes: {
        transform: `translate(0 ${axisSize.height})`,
      },
    });
  }

  function createLabels(): void {
    const { xScale, xOffset } = stateController.getState();
    const xLabelsStep = getXLabelsStep(xScale);
    const xLabels = getXLabels(xLabelsStep, xScale, xOffset);
    xLabels.forEach(timestamp => {
      const svgLabel = makeSvgLabel(timestamp);
      xSvgLabels.appendChild(svgLabel);
    });
  }

  function getXLabels(
    xLabelsStep: number,
    xScale: number,
    xOffset: number,
  ): Timestamp[] {
    const { xTotalRange } = stateController.getState();
    const xViewStart = xTotalRange.start + xOffset * xTotalRange.distance;
    const xViewEnd = xViewStart + xScale * xTotalRange.distance;

    const labels = [];
    const labelsToSkip = Math.floor((xTotalRange.end - xViewEnd) / xLabelsStep);
    const overhead = 2;
    const xLabelsEnd =
      xTotalRange.end - (labelsToSkip - overhead) * xLabelsStep;
    const xLabelsStart = xViewStart - overhead * xLabelsStep;

    let value = xLabelsEnd;
    while (value >= xLabelsStart) {
      labels.push(value);
      value = value - xLabelsStep;
    }

    return labels;
  }

  function updateLabels(): void {
    updateLabelsElements();
  }

  function updateLabelsElements() {
    const {
      xScale: newXScale,
      xOffset: newXOffset,
    } = stateController.getState();

    const newXLabelsStep = getXLabelsStep(newXScale);
    const newLabels = getXLabels(newXLabelsStep, newXScale, newXOffset);

    createMissingLabels(newLabels);
    updateLabelsAttributes(newXLabelsStep, newLabels);
  }

  function createMissingLabels(labels: Timestamp[]) {
    labels.forEach(timestamp => {
      const el = xSvgLabels.querySelector(`[data-value="${timestamp}"]`);
      if (!el) {
        const label = makeSvgLabel(timestamp);
        xSvgLabels.appendChild(label);
      }
    });
  }

  function updateLabelsAttributes(
    newXLabelsStep: number,
    newLabels: Timestamp[],
  ) {
    const {
      xTotalRange,
      xScale: newXScale,
      xOffset,
    } = stateController.getState();

    const svgElementsCount = xSvgLabels.children.length;
    const xOffsetValue = xTotalRange.start + xOffset * xTotalRange.distance;

    const elementsToRemove: SVGElement[] = [];

    for (let i = 0; i < svgElementsCount - 1; i = i + 1) {
      const svgLabel = <SVGElement>xSvgLabels.children[i];
      const timestamp = Number(svgLabel.getAttribute('data-value'));

      const shouldRemoveElement = newLabels.indexOf(timestamp) < 0;

      if (shouldRemoveElement) {
        elementsToRemove.push(svgLabel);
        continue;
      }

      const styleUpdates = { opacity: '1' };

      let attributesUpdates;
      let x =
        ((timestamp - xOffsetValue) * xCoordsScale) / newXScale -
        xTextLabelOffset;
      if (timestamp === xTotalRange.end) {
        x = x - xTextLabelOffset;
      }
      attributesUpdates = { x: `${x}` };

      applyElementConfig(svgLabel, {
        style: styleUpdates,
        attributes: attributesUpdates,
      });
    }

    if (elementsToRemove.length > 0) {
      elementsToRemove.forEach(element => xSvgLabels.removeChild(element));
    }
  }

  function makeSvgLabel(timestamp: Timestamp): SVGElement {
    const xSvgLabel = makeSvgElement('text', {
      classList: ['value'],
      attributes: {
        x: '0',
        y: '0',
        'data-value': `${timestamp}`,
      },
      style: { opacity: '0' },
    });
    xSvgLabel.innerHTML = getXLabelText(timestamp);
    return xSvgLabel;
  }

  function getXLabelText(xValue: Timestamp): string {
    const date = new Date(xValue);
    return date.toLocaleString('en-US', { month: 'short', day: '2-digit' });
  }

  function getXLabelsStep(xScale: number): number {
    const { xTotalRange } = stateController.getState();
    const xViewDistance = xScale * xTotalRange.distance;

    const preciseXStepDays = xViewDistance / xLabelsCount / dayRange;
    const pow2 = Math.ceil(Math.log2(preciseXStepDays));
    const xStepDays = pow2 < 1 ? 1 : Math.pow(2, pow2);

    return dayRange * xStepDays;
  }
}
