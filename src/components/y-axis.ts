import {
  IChartComponent,
  IChartStateController,
  DOMContainerElement,
  ISize,
} from './types';
import {
  applyElementConfig,
  formatNumber,
  makeLinearAnimation,
  makeSvgElement,
} from '../utils/ui';

interface IYAxisProps {
  plotSize: ISize;
}

export function makeYAxis(
  chartStateController: IChartStateController,
): IChartComponent<IYAxisProps> {
  const stateController = chartStateController;

  let svgAxis: SVGElement;
  let plotSize: ISize;

  // Y-axis
  let currentSvgYLabels: SVGElement;
  const yLabelTextOffset = 10;
  let yScale: number;
  let yOffset: number;
  let yLabels: number[];
  let yCoordsScale: number;

  return Object.freeze({
    render,
  });

  function render(container: DOMContainerElement, props: IYAxisProps): void {
    initAxisProps(props);
    renderAxis(container);
    attachHandlers();
  }

  function initAxisProps(props: IYAxisProps) {
    const chartState = stateController.getState();
    yScale = chartState.yScale;
    yOffset = chartState.yOffset;
    plotSize = props.plotSize;
    yLabels = chartState.yLabels;
    yCoordsScale = plotSize.height / chartState.yTotalRange.distance;
  }

  function renderAxis(container: DOMContainerElement) {
    svgAxis = makeSvgElement('g', {});
    currentSvgYLabels = renderYLabels(yLabels);
    container.appendChild(svgAxis);
  }

  function attachHandlers() {
    stateController.addListener(
      'yViewRangeChanged',
      handleYAxisViewRangeChanged,
    );
  }

  function handleYAxisViewRangeChanged() {
    const {
      yScale: targetYScale,
      yOffset: targetYOffset,
      yLabels: targetYLabels,
    } = stateController.getState();

    if (yScale === targetYScale && yOffset === targetYOffset) {
      return;
    }

    animateYAxisViewRangeChange(targetYScale, targetYOffset, targetYLabels);
  }

  function animateYAxisViewRangeChange(
    targetScale: number,
    targetOffset: number,
    targetYLabels: number[],
  ) {
    const nextSvgYLabels = renderYLabels(targetYLabels);
    applyElementConfig(nextSvgYLabels, { style: { opacity: '0' } });

    const animatedProps = [
      { start: yScale, end: targetScale },
      { start: yOffset, end: targetOffset },
      { start: 0, end: 1 },
    ];

    yScale = targetScale;
    yOffset = targetOffset;
    yLabels = targetYLabels;
    makeLinearAnimation(
      animatedProps,
      200,
      ([nextScale, nextOffset, nextOpacity]) => {
        const currentOpacity = 1 - nextOpacity;
        updateSvgYLabels(nextSvgYLabels, nextScale, nextOffset, nextOpacity);
        if (nextScale === targetScale) {
          svgAxis.removeChild(currentSvgYLabels);
          currentSvgYLabels = nextSvgYLabels;
        } else {
          updateSvgYLabels(
            currentSvgYLabels,
            nextScale,
            nextOffset,
            currentOpacity,
          );
        }
      },
    );
  }

  function updateSvgYLabels(
    labelsSvgContainer: SVGElement,
    nextScale: number,
    nextOffset: number,
    opacity: number,
  ): void {
    const length = labelsSvgContainer.children.length;
    for (let i = 0; i < length; i = i + 1) {
      const labelEl = <SVGElement>labelsSvgContainer.children[i];
      const value = Number(labelEl.getAttribute('data-value'));
      const yCoordinate = getYLabelCoordinate(value, nextOffset, 1 / nextScale);

      const attributes =
        labelEl.tagName === 'line'
          ? { y1: `${yCoordinate}`, y2: `${yCoordinate}` }
          : { y: `${getYLabelTextCoordinate(yCoordinate)}` };

      applyElementConfig(labelEl, { attributes });
    }

    applyElementConfig(labelsSvgContainer, {
      style: { opacity: `${opacity}` },
    });
  }

  function getYLabelCoordinate(
    value: number,
    yOffset: number,
    yScale: number,
  ): number {
    return -Math.round(yCoordsScale * yScale * (value - yOffset));
  }

  function getYLabelTextCoordinate(yLabelCoordinate: number): number {
    return yLabelCoordinate - yLabelTextOffset;
  }

  function renderYLabels(labels: number[]): SVGElement {
    const g = makeSvgElement('g', {
      classList: ['axis-labels'],
    });

    labels.forEach(label => {
      const yCoordinate = getYLabelCoordinate(label, yOffset, yScale);
      const labelSvgLine = makeSvgElement('line', {
        classList: ['label'],
        attributes: {
          x1: '0',
          y1: `${yCoordinate}`,
          x2: `${plotSize.width}`,
          y2: `${yCoordinate}`,
          'data-value': `${label}`,
        },
      });

      const labelSvgText = makeSvgElement('text', {
        attributes: {
          x: '0',
          y: `${getYLabelTextCoordinate(yCoordinate)}`,
          'data-value': `${label}`,
        },
      });

      labelSvgText.innerHTML = formatNumber(label);

      g.appendChild(labelSvgLine);
      g.appendChild(labelSvgText);
    });

    svgAxis.appendChild(g);
    return g;
  }
}
