export interface ISrcData {
  columns: SrcColumn[];
  types: ISrcTypes;
  names: ISrcNames;
  colors: ISrcColors;
}

export enum SrcTypes {
  line = 'line',
  x = 'x',
}

type ColumnsValues = String | Number;
type SrcColumn = ColumnsValues[];

interface ISrcTypes {
  [key: string]: SrcTypes;
}

interface ISrcNames {
  [key: string]: string;
}

interface ISrcColors {
  [key: string]: string;
}
