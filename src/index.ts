import { renderApp } from './app';
import chartsData from './data/chart_data.json';
import './styles/index.scss';

renderApp(chartsData);
