import { IBaseRange, IDataSet, IRange } from '../components/types';

export function getDataSetMinMax(
  dataSet: IDataSet,
  startIndex?: number,
  endIndex?: number,
): IBaseRange {
  let start = 0;
  let end = 1;

  const startAt = typeof startIndex === 'number' ? Math.max(startIndex, 0) : 0;
  const maxIndex = dataSet.values.length - 1;
  const endAt =
    typeof endIndex === 'number' ? Math.min(endIndex, maxIndex) : maxIndex;

  for (let i = startAt; i < endAt + 1; i = i + 1) {
    const value = dataSet.values[i];
    if (value < start) {
      start = value;
    }

    if (value > end) {
      end = value;
    }
  }

  return { start, end };
}

export function getAdjustedYRange(dataSet: IDataSet, stepsCount: number): IRange {
  const { start, end } = getDataSetMinMax(dataSet);
  const distance = end - start;
  return adjustRange({ start, end, distance }, stepsCount);
}

export function adjustRange(range: IRange, stepsCount: number) {
  const roughLabelStep = range.distance / stepsCount;
  const y = Math.ceil(Math.log10(roughLabelStep) - 1);
  const pow10y = Math.pow(10, y);
  const labelStep = Math.ceil(roughLabelStep / pow10y) * pow10y;

  const adjustedStart = labelStep * Math.floor(range.start / labelStep);
  const adjustedEnd = adjustedStart + labelStep * stepsCount;

  return {
    start: adjustedStart,
    end: adjustedEnd,
    distance: adjustedEnd - adjustedStart,
  };
}
