export interface IAttributesMap {
  [key: string]: string;
}

const svgNS = 'http://www.w3.org/2000/svg';

export interface IElementConfig {
  classList?: string[];
  style?: IAttributesMap;
  attributes?: IAttributesMap;
}

export function makeSvgRootElement(elementConfig: IElementConfig) {
  const configAttributes = elementConfig.attributes || {};
  const extendedAttributes = {
    version: '1.1',
    xmlns: svgNS,
    ...configAttributes,
  };
  return makeSvgElement('svg', {
    ...elementConfig,
    attributes: extendedAttributes,
  });
}

export function makeSvgElement(
  elementName: string,
  elementConfig?: IElementConfig,
): SVGElement {
  const element = document.createElementNS(svgNS, elementName);

  if (elementConfig) {
    applyElementConfig(element, elementConfig);
  }

  return element;
}

export function makeHtmlElement(
  elementName: string,
  elementConfig: IElementConfig,
): HTMLElement {
  const element = document.createElement(elementName);

  if (elementConfig) {
    applyElementConfig(element, elementConfig);
  }

  return element;
}

type SvgOrHtmlElement = SVGElement | HTMLElement;

export function applyElementConfig(
  element: SvgOrHtmlElement,
  elementConfig: IElementConfig,
): void {
  const { classList, style, attributes } = elementConfig;

  if (classList && classList.length > 0) {
    classList.forEach(className => element.classList.add(className));
  }

  if (style) {
    // @ts-ignore
    Object.keys(style).forEach(key => (element.style[key] = style[key]));
  }

  if (attributes) {
    Object.keys(attributes).forEach(key =>
      element.setAttribute(key, attributes[key]),
    );
  }
}

export function toPx(value: number) {
  return `${value}px`;
}

interface IAnimatedProp {
  start: number;
  end: number;
}

type AnimationStepHandler = (animatedProps: number[]) => void;

export function makeLinearAnimation(
  animatedProps: IAnimatedProp[],
  duration: number,
  stepHandler: AnimationStepHandler,
  onAnimationEnd?: () => void,
): () => void {
  const animationStart = Date.now();
  const diffs = animatedProps.map(({ start, end }) => end - start);

  let animationStepId: any;

  const step = () => {
    const timeStamp = Date.now() - animationStart;

    if (timeStamp >= duration) {
      const finalProps = animatedProps.map(prop => prop.end);
      stepHandler(finalProps);
      onAnimationEnd && onAnimationEnd();
      return;
    }

    const relDuration = timeStamp / duration;
    const nextProps = animatedProps.map(
      ({ start }, index) => start + diffs[index] * relDuration,
    );
    stepHandler(nextProps);

    animationStepId = window.requestAnimationFrame(step);
  };

  animationStepId = window.requestAnimationFrame(step);
  return () => window.cancelAnimationFrame(animationStepId);
}

export function formatNumber(value: number): string {
  const scales: { [key: number]: string } = {
    3: 'k',
    6: 'M',
    9: 'B',
  };

  const precision = 100;
  const pow10 = Math.log10(value);
  const scale = Math.floor(pow10 / 3) * 3;
  if (!scales[scale]) {
    return `${limitPrecision(value, precision)}`;
  }

  const scaledValue = limitPrecision(value / Math.pow(10, scale), precision);
  return `${scaledValue}${scales[scale]}`;
}

function limitPrecision(value: number, precision: number): number {
  return Math.round(value * precision) / precision;
}
